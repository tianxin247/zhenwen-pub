import React, { Component } from "react";
import HeaderRB from "./HeaderRB";
import Footer from "./Footer";

import BreadcrumbRB from "./BreadcrumbRB";
import CreditClaim from "./CreditClaim";
import ModalAppIntro from "./ModalAppIntro";
import FormInputs from "./FormInputs";
import ScrollToTopBtn from "./ScrollToTop";
import AppConfig from "../data/appConfig";
import "./App.css";

// data file
import "../data/zhenwen.js";

const appTitle = AppConfig["AppSet"]["title_c"];

const parentLink = {
	title: AppConfig["OSASet"]["title_c"],
	link: AppConfig["OSASet"]["link"],
};

class App extends Component {
	render() {
		return (
			<div>
				<HeaderRB appTitle={appTitle} />
				<BreadcrumbRB actEntry={appTitle} parentEntry={parentLink} />
				<ModalAppIntro modTitle={appTitle} />
				<FormInputs appTitle={appTitle} />
				<CreditClaim />
				<ScrollToTopBtn />
				<Footer />
			</div>
		);
	}
}

export default App;
