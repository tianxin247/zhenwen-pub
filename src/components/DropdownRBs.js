import React, { useState } from "react";
import {
	Dropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
} from "reactstrap";
import OSAList from "../data/osaList";

const DropdownRBs = () => {
	const [dropdownOpen, setDropdownOpen] = useState(false);

	const toggle = () => setDropdownOpen((prevState) => !prevState);

	const listSource = OSAList;

	return (
		<Dropdown isOpen={dropdownOpen} toggle={toggle}>
			<DropdownToggle caret>--- 檢索列表 ---</DropdownToggle>
			<DropdownMenu>
				{listSource.map((item) => (
					<DropdownItem key={item.id}>
						<a href={item.link}>{item.title}</a>
					</DropdownItem>
				))}
			</DropdownMenu>
		</Dropdown>
	);
};

export default DropdownRBs;
