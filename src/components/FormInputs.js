import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { Button, Form, FormGroup, Label, Input } from "reactstrap";
import { Table } from "reactstrap";
import AppConfig from "../data/appConfig";

function fill(results) {
	document.getElementById("results_table").style.display = "block";

	var table = document.getElementById("searchTableOut");
	// 清除网页上旧的查询结果
	for (var i = table.rows.length - 1; i >= 0; i--) {
		table.deleteRow(i);
	}

	// Set search results message
	let search_results = "";
	if (results.length == 0) {
		search_results = "沒有找到。請重新檢索。不支持簡化漢字檢索。";
		document.getElementById("search_results").innerHTML = search_results;
		document.getElementById("results_table").style.display = "none";
	} else {
		search_results =
			"檢出 " +
			results.length +
			" 條: 按 Ctrl+F 鍵，可搜索當前頁，高亮全部，點上下箭頭查閱。";
	}
	document.getElementById("search_results").innerHTML = search_results;

	// 将查询结果填入表格
	for (var x = 0; x < results.length; x++) {
		var result = results[x];
		var row = document.createElement("tr"); //创建一行
		// ASCII 中 97-106 分别对应 a-j，网页上对应从左至右的各列
		// 用 String.fromCharCode 将数字转为对应的英文字符，如 String.fromCharCode(97) 值是 a
		for (var d = 97; d <= 99; d++) {
			var cell = document.createElement("td");
			var str = result[String.fromCharCode(d)];
			if (d == 97) {
				str =
					"<a href=./orgpage.html?page=" +
					str +
					" target=hvWin>" +
					str +
					"</a>";
			}
			cell.innerHTML = str;
			row.appendChild(cell);
		}
		table.appendChild(row);
	}
}

/**
 * 查询函数
 * @id: 网页中输入栏的 id(queryString7 等等)
 * @field: 数据集中列的 field(dcd1.js 中的 a-j)
 */
function search(id, field) {
	var results = [];
	var value = document.getElementById(id).value;
	if (!field || !value) {
		return;
	}
	// 拼音注音模式
	var pinyin_pattern = new RegExp(value + "\\d*$");
	// 模糊匹配
	var pattern = new RegExp(value);
	// 依次查找，符合条件的加入 results 中
	for (var i = 0; i < books.length; i++) {
		switch (field) {
			case "c":
			case "": // 字头
				var fieldValue = books[i][field];
				if (fieldValue.match(pattern)) {
					results.push(books[i]);
				}
				break;
			case "b":
			case "": // 字头编号
				var fieldValue = books[i][field];
				if (fieldValue == value) {
					results.push(books[i]);
				}
				break;
			case "": // 反切
				var fieldValue = books[i][field];
				if (fieldValue == value + "切") {
					results.push(books[i]);
				}
				break;
			case "": // 注音
			case "": // 拼音
				var fieldValues = books[i][field].split("/");
				for (var j = 0; j < fieldValues.length; j++) {
					if (fieldValues[j].match(pinyin_pattern)) {
						results.push(books[i]);
					}
				}
				break;
			case "": //部首
				var fieldValues = books[i][field].split("_");
				for (var j = 0; j < fieldValues.length; j++) {
					if (fieldValues[j].match(pattern)) {
						results.push(books[i]);
					}
				}
				break;
		}
	}
	fill(results);
}

const placeholder_prompt =
	' -> 可參考 "' + AppConfig["AppSet"]["title_c"] + '使用說明"';

const frmStyle = {
	maxWidth: 400,
	marginLeft: "auto",
	marginRight: "auto",
	textAlign: "center",
};

const ctrStyle = {
	marginLeft: "auto",
	marginRight: "auto",
	textAlign: "center",
};

const rstStyle = {
	textAlign: "center",
	color: "red",
};

const rstTblStyle = {
	marginLeft: "auto",
	marginRight: "auto",
	maxWidth: 700,
};

class FormInputs extends Component {
	constructor(props) {
		super(props);
		this.state = { selectedOption: "" };
		this.onValueChange = this.onValueChange.bind(this);
		this.formSubmit = this.formSubmit.bind(this);
	}

	onValueChange(event) {
		this.setState({
			selectedOption: event.target.value,
		});
	}

	formSubmit(event) {
		event.preventDefault();
		if (!this.state.selectedOption) {
			alert("請先選檢索方式");
			return;
		}
		let selected_field = "";
		if (this.state.selectedOption === "漢字") {
			selected_field = "b";
		} else if (this.state.selectedOption === "眞文") {
			selected_field = "c";
		}
		let selected_id = "content";
		let value = document.getElementById("content").value;
		if (!value) {
			alert("請輸入檢索內容");
			return;
		}
		search(selected_id, selected_field);
	}

	render() {
		return (
			<React.Fragment>
				<div>
					<Form style={frmStyle} onSubmit={this.formSubmit}>
						<FormGroup tag="fieldset">
							<FormGroup row>
								<Label
									for="radio1"
									className="col-form-label col-sm-3"
								>
									<b>檢索方式</b>
								</Label>
								<Col
									sm={9}
									className="form-check-inline"
									style={ctrStyle}
								>
									<FormGroup check>
										<Label check>
											<Input
												type="radio"
												name="radio1"
												id="title"
												value="漢字"
												checked={
													this.state
														.selectedOption ===
													"漢字"
												}
												onChange={this.onValueChange}
											/>{" "}
											漢字
										</Label>
									</FormGroup>
									<FormGroup check>
										<Label check>
											<Input
												type="radio"
												name="radio1"
												id="chap_num"
												value="眞文"
												checked={
													this.state
														.selectedOption ===
													"眞文"
												}
												onChange={this.onValueChange}
											/>{" "}
											眞文
										</Label>
									</FormGroup>
								</Col>
							</FormGroup>
							<FormGroup row>
								<Label for="content" sm={3}>
									<b>檢索內容</b>
								</Label>
								<Col sm={9}>
									<Input
										type="text"
										name="content"
										id="content"
										placeholder={
											this.state.selectedOption +
											placeholder_prompt
										}
									/>
								</Col>
							</FormGroup>
							<Button type="submit">搜索</Button>
						</FormGroup>
					</Form>
				</div>

				<div style={rstStyle}>
					<p id="search_results"></p>
				</div>

				<div style={rstTblStyle}>
					<Table
						id="results_table"
						striped
						bordered
						style={{ display: "none" }}
					>
						<thead>
							<tr className="tbl-header">
								<th style={{ width: "10%" }}>原書</th>
								<th style={{ width: "10%" }}>漢字</th>
								<th style={{ width: "80%" }}>
									道敎眞文（天文、靈文、雷文、天篆、雲篆、雷篆）
								</th>
							</tr>
						</thead>
						<tbody id="searchTableOut" className="tbl-body"></tbody>
					</Table>
				</div>
			</React.Fragment>
		);
	}
}

export default FormInputs;
