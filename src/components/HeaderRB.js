import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import DropdownRBs from "./DropdownRBs";
import siteLogo from "../assets/hims-site-logo_180.jpg";
import AppConfig from "../data/appConfig";
import "./App.css";

const siteURL = AppConfig["HIMSUSSite"]["link"];

const style = {
	backgroundColor: "#c7edcc",
	padding: "20px 10px 10px",
};

const titleStyle = {
	color: "#008000",
	textAlign: "center",
};

const HeaderRB = (props) => {
	return (
		<Container fluid={true} style={style}>
			<Row>
				<Col xs="12" sm="4" className="left-col">
					<a href={siteURL}>
						<img src={siteLogo} />
					</a>
				</Col>
				<Col xs="12" sm="4">
					<h1 style={titleStyle}>{props.appTitle}</h1>
				</Col>
				<Col xs="12" sm="4" className="right-col">
					<DropdownRBs />
				</Col>
			</Row>
		</Container>
	);
};

export default HeaderRB;
