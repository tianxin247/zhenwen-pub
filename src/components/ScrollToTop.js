import React, { Component } from "react";

export default class ScrollToTop extends Component {
	constructor(props) {
		super(props);
		this.state = {
			is_visible: false,
		};
	}

	componentDidMount() {
		var scrollComponent = this;
		document.addEventListener("scroll", function (e) {
			scrollComponent.toggleVisibility();
		});
	}

	toggleVisibility() {
		if (window.pageYOffset > 300) {
			this.setState({
				is_visible: true,
			});
		} else {
			this.setState({
				is_visible: false,
			});
		}
	}

	scrollToTop() {
		window.scrollTo({
			top: 0,
			behavior: "smooth",
		});
	}

	render() {
		const { is_visible } = this.state;
		return (
			<div className="scroll-to-top">
				{is_visible && (
					<div onClick={() => this.scrollToTop()}>
						<svg
							xmlns="http://www.w3.org/2000/svg"
							viewBox="0 0 24 24"
							fill="blue"
							stroke="currentColor"
							strokeWidth="2"
							strokeLinecap="round"
							strokeLinejoin="round"
							className="feather feather-arrow-up-circle"
						>
							<circle cx="12" cy="12" r="10"></circle>
							<polyline points="16 12 12 8 8 12"></polyline>
							<line x1="12" y1="16" x2="12" y2="8"></line>
						</svg>

						{/*
                            https://freeicons.io/business-and-online-icons/arrow-up-circle-icon-icon
                        */}
					</div>
				)}
			</div>
		);
	}
}
