import React from "react";
import { Container, Row, Col } from "reactstrap";
import AppConfig from "../data/appConfig";

const usSiteURL = AppConfig["HIMSUSSite"]["link"];
const usSiteTitleC = AppConfig["HIMSUSSite"]["title_c"];
const hvSiteURL = AppConfig["HyviewSite"]["link"];
const hvSiteTitle = AppConfig["HyviewSite"]["title"];

var contactUsUS =
	"/%E9%97%9C%E6%96%BC%E7%B6%B2%E7%AB%99/%E8%81%AF%E7%B5%A1%E6%88%91%E5%80%91/";
contactUsUS = usSiteURL + contactUsUS;
const contactUsC = "聯絡我們";

var contactUsHv = "/site/contact-us/%e8%81%af%e7%b5%a1%e6%88%91%e5%80%91/";
contactUsHv = hvSiteURL + contactUsHv;

const copyrightStart = "2019";
const thisDay = new Date();
const copyrightThisYear = thisDay.getFullYear();

const style = {
	backgroundColor: "#c7edcc",
	margin: "15px 0 0",
	padding: "15px 10px 15px",
};

const Footer = () => {
	return (
		<Container fluid={true} style={style}>
			<Row>
				<Col xs="12" sm="12" md="8" className="left-col">
					<span>
						&copy; {usSiteTitleC} {copyrightStart} -{" "}
						{copyrightThisYear}{" "}
						<a href={contactUsUS}>{contactUsC}</a>
					</span>
				</Col>
				<Col xs="12" sm="12" md="4" className="right-col">
					<span>
						Designed by{" "}
						<a href={contactUsHv} target="_blank">
							{hvSiteTitle}
						</a>
					</span>
				</Col>
			</Row>
		</Container>
	);
};

export default Footer;
