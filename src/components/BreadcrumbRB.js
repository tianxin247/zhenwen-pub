import React from "react";
import { Breadcrumb, BreadcrumbItem } from "reactstrap";
import AppConfig from "../data/appConfig";
import LangUI from "../data/langUI";

const usSiteURL = AppConfig["HIMSUSSite"]["link"];
const homepageC = LangUI["home"]["tc"];

function WithParent(props) {
	return (
		<div>
			<Breadcrumb className="breadcrumb-custom">
				<BreadcrumbItem>
					<a href={usSiteURL}>{homepageC}</a>
				</BreadcrumbItem>
				<BreadcrumbItem>
					<a href={props.parentEntry.link}>
						{props.parentEntry.title}
					</a>
				</BreadcrumbItem>
				<BreadcrumbItem active>{props.actEntry}</BreadcrumbItem>
			</Breadcrumb>
		</div>
	);
}

function WithoutParent(props) {
	return (
		<div>
			<Breadcrumb className="breadcrumb-custom">
				<BreadcrumbItem>
					<a href={usSiteURL}>{homepageC}</a>
				</BreadcrumbItem>
				<BreadcrumbItem active>{props.actEntry}</BreadcrumbItem>
			</Breadcrumb>
		</div>
	);
}

const BreadcrumbRB = (props) => {
	if (!props.parentEntry) {
		return <WithoutParent actEntry={props.actEntry} />;
	} else {
		return (
			<WithParent
				actEntry={props.actEntry}
				parentEntry={props.parentEntry}
			/>
		);
	}
};

export default BreadcrumbRB;
