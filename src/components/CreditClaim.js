import React from "react";
import AppConfig from "../data/appConfig";

const style = {
	textAlign: "center",
	marginTop: 15,
};

const himsSiteTitle = AppConfig["HIMSSite"]["title_c"];
const founder = {
	title: AppConfig["HIMSFounder"]["title_c"],
	link: AppConfig["HIMSFounder"]["link"],
};

const CreditClaim = () => {
	return (
		<p style={style}>
			{himsSiteTitle}{" "}
			<a href={founder.link} target="_blank">
				{founder.title}
			</a>{" "}
			整理
		</p>
	);
};

export default CreditClaim;
