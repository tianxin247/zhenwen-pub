import React, { Component } from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import ModalContent from "./ModalContent";
import LangUI from "../data/langUI";

const divStyle = {
	marginTop: -10,
	marginLeft: 10,
};

const langClose = LangUI["close"]["tc"];

class ModalAppInto extends Component {
	constructor(props) {
		super(props);
		this.state = {
			modal: false,
		};

		this.toggle = this.toggle.bind(this);
	}

	toggle() {
		this.setState((prevState) => ({
			modal: !prevState.modal,
		}));
	}

	render() {
		const modTitle = this.props.modTitle + "使用說明";

		return (
			<div style={divStyle}>
				<Button color="info" onClick={this.toggle}>
					{modTitle}
				</Button>
				<Modal
					isOpen={this.state.modal}
					toggle={this.toggle}
					className={this.props.className}
				>
					<ModalHeader toggle={this.toggle}>
						<b>{modTitle}</b>
					</ModalHeader>
					<ModalBody>
						<ModalContent />
					</ModalBody>
					<ModalFooter>
						<Button color="secondary" onClick={this.toggle}>
							{langClose}
						</Button>
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}

export default ModalAppInto;
