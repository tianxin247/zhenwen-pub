import React from "react";
import ReactDOM from "react-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import App from "./components/App";
import AppConfig from "./data/appConfig";
import "./index.css";

// update Page Title
const siteTitle = AppConfig["HIMSUSSite"]["title_c"];
const appTitle = AppConfig["AppSet"]["title_c"];
const pageTitle = appTitle + " - " + siteTitle;
document.getElementsByTagName("TITLE")[0].text = pageTitle;

ReactDOM.render(<App />, document.getElementById("root"));
