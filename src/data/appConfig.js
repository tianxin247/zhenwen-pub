/*	
	App Configuration file
	Date created: 8/10/2020; Last Update: 8/28/2020
*/

const usHomeURL = "https://homeinmists.ilotus.org";

const AppConfig = {
	HIMSSite: {
		title: "HIMS Source Site",
		title_c: "白雲深處人家",
		link: "http://www.homeinmists.com",
	},
	HIMSFounder: {
		title: "Jiang Menma",
		title_c: "蔣門馬",
		link:
			usHomeURL +
			"/%e9%97%9c%e6%96%bc%e7%b6%b2%e7%ab%99/%e7%b6%b2%e7%ab%99%e5%89%b5%e5%bb%ba%e4%ba%ba/",
	},
	HIMSUSSite: {
		title: "HIMS US Site",
		title_c: "白雲深處人家海外站",
		link: usHomeURL,
	},
	HyviewSite: {
		title: "Hyview Softech",
		title_c: "高瞻",
		link: "https://www.hyview.com",
	},
	OSASet: {
		title: "Online Search App",
		title_c: "在線檢索",
		link:
			usHomeURL +
			"/%e7%b6%b2%e7%ab%99%e5%85%ac%e5%91%8a/%e5%9c%a8%e7%b7%9a%e6%aa%a2%e7%b4%a2/",
	},
	AppSet: {
		title: "Zhenwen Dictionary",
		title_c: "道敎眞文字典",
		link: usHomeURL + "/zhenwen/",
	},
};

export default AppConfig;
