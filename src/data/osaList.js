{
	/*	
	OSA: Online Search Apps
	Date created: 8/10/2020; Last Update: 8/28/2020
	
	The link here is relative to site root. 
	To get complete link, append it to siteURL = "https://homeinmists.ilotus.org"
	*/
}

const usHomeURL = "https://homeinmists.ilotus.org";

const OSAList = [
	{
		id: 1,
		title: "網站綜合檢索",
		link: usHomeURL + "/search/",
	},
	{
		id: 2,
		title: "周易",
		link: usHomeURL + "/DaoD/zhouyi.php",
	},
	{
		id: 3,
		title: "老子",
		link: usHomeURL + "/DaoD/laozi.php",
	},
	{
		id: 4,
		title: "莊子",
		link: usHomeURL + "/DaoD/zhuangzi/",
	},
	{
		id: 5,
		title: "論語",
		link: usHomeURL + "/DaoD/lunyu/",
	},
	{
		id: 6,
		title: "道藏提要",
		link: usHomeURL + "/DaoZang/DaoZang.php",
	},
	{
		id: 7,
		title: "道教大辭典",
		link: usHomeURL + "/DaoD/search.php",
	},
	{
		id: 8,
		title: "道敎眞文字典",
		link: usHomeURL + "/zhenwen/",
	},
	{
		id: 9,
		title: "漢英道教詞典",
		link: usHomeURL + "/DaoD/find.php",
	},
	{
		id: 10,
		title: "漢語大詞典",
		link: usHomeURL + "/hd/hydcd.php",
	},
	{
		id: 11,
		title: "漢語大字典",
		link: usHomeURL + "/hd/hydzd.php",
	},
	{
		id: 12,
		title: "更多...",
		link:
			usHomeURL +
			"/%e7%b6%b2%e7%ab%99%e5%85%ac%e5%91%8a/%e5%9c%a8%e7%b7%9a%e6%aa%a2%e7%b4%a2/",
	},
];

export default OSAList;
