{
	/*	
	Language User Interface Terms
	Date created: 8/11/2020; Last Update: 8/11/2020
	*/
}

const LangUI = {
	home: {
		en: "Home",
		tc: "首頁",
		sc: "首页",
	},
	close: {
		en: "Close",
		tc: "關閉",
		sc: "关闭",
	},
	contact_us: {
		en: "Contact Us",
		tc: "聯絡我們",
		sc: "联络我们",
	},
};

export default LangUI;
